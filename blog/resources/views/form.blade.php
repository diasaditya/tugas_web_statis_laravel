<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/kirim" method="POST">
        @csrf
    <label for="First name">First name :</label>
    <br><br>
    <input type="text" name= "namadepan">
    <br><br>
    <label for="Last name">Last name :</label>
    <br><br>
    <input type="text" name="namabelakang">
    <br><br>
    <label for="Gender">Gender :</label>
    <br><br>
    <input type="radio" id = "Male" name ="Gender"><label for="Male">Male</label>
    <br>
    <input type="radio" id = "Female" name ="Gender"><label for="Female">Female</label>
    <br>
    <input type="radio" id = "Other" name ="Gender"><label for="Other">Other</label>
    <br><br>
    <label for="Nationality">Nationality:</label>
    <br><br>
    <select name="" id="">
        <option value="">Indonesian</option>
        <option value="">Singapure</option>
        <option value="">Malaysian</option>
        <option value="">Indian</option>
    </select>
    <br><br>
    <label for="Language Spoken">Language Spoken:</label>
    <br><br>
    <input type="checkbox" id = "Bahasa Indonesia" name ="Language Spoken"><label for="Bahasa Indonesia">Bahasa Indonesia</label>
    <br>
    <input type="checkbox" id = "English" name ="Language Spoken"><label for="English">English</label>
    <br>
    <input type="checkbox" id = "Other" name ="Language Spoken"><label for="Other">Other</label>
    <br><br>
    <label for="">Bio:</label>
    <br><br>
    <textarea cols="30" rows="10"></textarea>
    <br>
    <input type="submit" value = "Kirim">
</form>

</body>
</html>